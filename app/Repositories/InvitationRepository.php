<?php

namespace App\Repositories;

use App\Models\Invitation;
use Illuminate\Http\Request;
use Auth;
/**
 * Class TransactionRepository.
 */
class InvitationRepository
{
    public function generate(Request $request)
    {
        $invitation = Invitation::create([
            'invitation' => Helper::generateInvitation(10),
            'user_id'    => Auth::user()->id,
        ]);

        return $invitation;
    }
}
