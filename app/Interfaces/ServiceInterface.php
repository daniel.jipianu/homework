<?php

namespace App\Interfaces;

use Illuminate\Http\JsonResponse;

interface ServiceInterface
{
    public function getTransactionsToJson(): JsonResponse;
}
