<?php

namespace App\Http\Service;

use App\Interfaces\ServiceInterface;
use Illuminate\Http\JsonResponse;
use App\Models\Transactions;

class DatabaseService implements ServiceInterface
{

    public function getTransactionsToJson(): JsonResponse
    {
        $data = Transactions::all();

        return response()->json($data);
    }
}
