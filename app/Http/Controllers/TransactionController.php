<?php

namespace App\Http\Controllers;

use App\Exceptions\UnknownServiceSourceException;
use App\Http\Service\ServiceFactory;
use Illuminate\Http\Request;
use App\Repositories\TransactionRepository;
use Exception;

class TransactionController extends Controller
{

    public function __construct(TransactionRepository $transaction)
    {
        $this->transactionRepository = $transaction;
    }

    public function transactions(Request $request)
    {
        $source = $request::get('source');

        try {
            $service = ServiceFactory::createService($source);
        } catch(UnknownServiceSourceException $exception) {
            abort(400);
        }

        return $service->getTransactionsToJson();
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'amount' => 'required',
        ]);

        try {
            $data = $this->transactionRepository->create($request);
            return response()->json(['data' => $data]);
        }  catch(Exception $e){
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'amount' => 'required',
        ]);

        try {
            $data = $this->transactionRepository->updateById($id, $request);
            return response()->json(['data' => $data]);
        } catch(Exception $e){
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function delete($id)
    {
        try {
            $data = $this->transactionRepository->deleteById($id);
            return response()->json(['data' => $data]);
        } catch(Exception $e){
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
