<?php

namespace App\Repositories;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Auth;
/**
 * Class TransactionRepository.
 */
class TransactionRepository
{
    /**
     * @return string
     *  Return the model
     */

    public function create(Request $request)
    {
        $data = Transaction::create([
            'code'      => $request->input('code'),
            'amount'    => $request->input('amount'),
            'user_id'   => Auth::user()->id
        ]);

        return $data;
    }

    public function updateById($id, Request $request)
    {
        $data = Transaction::findOrFail($id);

        $requestData = ([
            'code'      => $request->input('code'),
            'amount'    => $request->input('amount'),
            'user_id'   => Auth::user()->id
        ]);

        $data->update($requestData);

        return $data;

    }

    public function deleteById($id)
    {
        $data = Transaction::findOrFail($id);
        $data->delete();

        return $data;
    }
}
