<?php

namespace App\Http\Service;

use App\Exceptions\UnknownServiceSourceException;
use App\Interfaces\ServiceInterface;

class ServiceFactory
{

    public static function createService(string $source): ServiceInterface
    {
        if ($source === 'db') {
            return new DatabaseService();
        } else if($source === 'csv') {
            return new CSVService();
        } else {
            throw new UnknownServiceSourceException();
        }
    }
}
