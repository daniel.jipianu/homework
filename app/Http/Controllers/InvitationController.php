<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\InvitationRepository;
use App\Invitation;

class InvitationController extends Controller
{

    public function __construct(InvitationRepository $invitation)
    {
        $this->InvitationRepository = $invitation;
    }

    public function invitations()
    {
        $data = Invitation::all();

        return response()->json(['data' => $data]);
    }

    public function create(Request $request)
    {
        $data = $this->invitationRepository->generate($request);

        return response()->json(['data' => $data]);
    }
}
