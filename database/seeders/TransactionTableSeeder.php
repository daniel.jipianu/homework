<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'public/transactions.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Done');
    }
}
