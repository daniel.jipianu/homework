<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use HasFactory;

    protected $fillable = ['invitation', 'user_id'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
