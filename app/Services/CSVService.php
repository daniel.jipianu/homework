<?php

namespace App\Http\Service;

use App\Interfaces\ServiceInterface;
use Illuminate\Http\JsonResponse;
use League\Csv\Reader;

class CSVService implements ServiceInterface
{

    public function getTransactionsToJson(): JsonResponse
    {
        $csv = Reader::createFromPath('../storage/transactions.csv');
        $csv = $csv->setOffset(1);
        $json = json_encode($csv, JSON_PRETTY_PRINT);
        return response($json);
    }
}
